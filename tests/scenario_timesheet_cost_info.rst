==========================
Timesheet Cost Info manage
==========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> today = datetime.date.today()


Install stock_unit_load_quality_control Module::

    >>> config = activate_modules('timesheet_cost_info')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create timesheet lines::

    >>> Employee = Model.get('company.employee')
    >>> Party = Model.get('party.party')
    >>> Work = Model.get('timesheet.work')
    >>> Line = Model.get('timesheet.line')
    >>> party = Party(name='Empleado 1')
    >>> party.save()
    >>> employee = Employee(company=company, party=party)
    >>> employee.save()
    >>> price = employee.cost_prices.new()
    >>> price.date = today
    >>> price.cost_price = Decimal(8.50)
    >>> employee.save()
    >>> work = Work(name='Work 1')
    >>> work.save()
    >>> line = Line(employee=employee, date=today, work=work, duration=datetime.timedelta(hours=8))
    >>> line.save()
    >>> line.cost_amount == 68.0
    True

Check amount field::

    >>> HoursEmployee = Model.get('timesheet.hours_employee')
    >>> hours_employee, = HoursEmployee.find([])
    >>> hours_employee.amount == line.cost_price * Decimal(str(line.duration.total_seconds() / 3600))
    True
